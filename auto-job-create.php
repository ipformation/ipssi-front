<?php
require_once('wp-load.php');

$id = $_GET['id'];
$url = 'http://projet-ipssi.com/app_dev.php/offer/rest/offer/'.$id;

/* gets the CVs from a URL */
$ch = curl_init();
$timeout = 5;
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$data = curl_exec($ch);
curl_close($ch);

$datadecode = json_decode($data);

if ($datadecode->available){
    $offer_post = array(
        'post_author' => 1,
        'post_type' => 'application',
        'post_title' => $datadecode->title,
        'post_name' => $datadecode->title,
        'post_status' => 'publish',
    );

    $post_id = wp_insert_post($offer_post);
    update_post_meta( $post_id, 'content', $datadecode->description );
    update_post_meta( $post_id, 'title', $datadecode->title );
    update_post_meta( $post_id, 'offer_id', $id );

} else {

    $offer_post = array(
        'post_author' => 1,
        'post_type' => 'application',
        'post_title' => $datadecode->title,
        'post_name' => $datadecode->title,
        'post_status' => 'pending',
    );

    $post_id = wp_insert_post($offer_post);
    update_post_meta( $post_id, 'content', $datadecode->description );
    update_post_meta( $post_id, 'title', $datadecode->title );
    update_post_meta( $post_id, 'offer_id', $id );
}




