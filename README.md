Penser à créer le vhost suivant : 

**Mise en place du vhost** :

```
cd /etc/apache2/sites-available/
sudo touch ipssi-front.com.conf
sudo nano ipssi-front.com.conf

<VirtualHost *:80>
    ServerName ipssi-front.com
    ServerAlias www.ipssi-front.com

    DocumentRoot /var/www/html/ipssi-front/
    <Directory /var/www/html/ipssi-front/>
        AllowOverride All
        Order Allow,Deny
        Allow from All
    </Directory>

    ErrorLog /var/log/apache2/project_error.log
    CustomLog /var/log/apache2/project_access.log combined
</VirtualHost>

sudo a2ensite ipssi-front.com.conf

sudo nano /etc/hosts
Add :
127.0.0.1 ipssi-front.com

sudo nano /etc/apache2/apache2.conf
Add :
ServerName localhost

sudo apt-get install php5-curl
sudo apt-get install php5-intl

sudo a2enmod rewrite

sudo service apache2 restart

```

=====

login : ipssi
mdp : 0000

Google site verification

Clé du site
Utilisez cette clé dans le code HTML que vous proposez à vos utilisateurs.
6Lfb2yITAAAAABAoOyrP_wtfkKnZic7qB3Dpvid_
Clé secrète
Utilisez cette clé pour toute communication entre votre site et Google. Veillez à ne pas la divulguer, car il s'agit d'une clé secrète.
6Lfb2yITAAAAAH76wxKd2QeIWlxcq1LHo9FjZatI
Étape 1 : intégration côté client
Collez cet extrait avant la balise fermante </head> sur votre modèle HTML :
<script src='https://www.google.com/recaptcha/api.js'></script>
Collez cet extrait après la balise <form>, là où vous souhaitez que le widget reCAPTCHA s'affiche :
<div class="g-recaptcha" data-sitekey="6Lfb2yITAAAAABAoOyrP_wtfkKnZic7qB3Dpvid_"></div>
Le site de documentation reCAPTCHA propose des informations plus détaillées et des configurations avancées.
Étape 2 : intégration côté serveur
Lorsque vos utilisateurs envoient le formulaire dans lequel vous avez intégré reCAPTCHA, vous recevez une chaîne de texte intitulée "g-recaptcha-response" parmi les données utiles. Pour savoir si cet utilisateur a été validé par Google, envoyez une demande POST avec les paramètres suivants :
URL : https://www.google.com/recaptcha/api/siteverify
secret (obligatoire)	6Lfb2yITAAAAAH76wxKd2QeIWlxcq1LHo9FjZatI
response (obligatoire)	La valeur "g-recaptcha-response"
remoteip	Adresse IP de l'utilisateur final