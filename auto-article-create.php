<?php
require_once('wp-load.php');

$id = $_GET['id'];

$url = 'http://projet-ipssi.com/app_dev.php/article/rest/article/'.$id;

/* gets the Article from a URL */
$ch = curl_init();
$timeout = 5;
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$data = curl_exec($ch);
curl_close($ch);

$datadecode = json_decode($data);

$filename = $datadecode->webPath;

if ($datadecode->published){
    $article_post = array(
        'post_author' => 1,
        'post_type' => 'article',
        'post_title' => $datadecode->title,
        'post_name' => $datadecode->title,
        'post_status' => 'publish',
    );

    $post_id = wp_insert_post($article_post);

    update_post_meta( $post_id, 'description', $datadecode->description );
    update_post_meta( $post_id, 'img_url', $datadecode->webPath );
    update_post_meta( $post_id, 'article_id', $id );

} else {
    $article_post = array(
        'post_author' => 1,
        'post_type' => 'article',
        'post_title' => $datadecode->title,
        'post_name' => $datadecode->title,
        'post_status' => 'pending',
    );

    $post_id = wp_insert_post($article_post);
    update_post_meta( $post_id, 'description', $datadecode->description );
    update_post_meta( $post_id, 'img_url', $datadecode->webPath );
    update_post_meta( $post_id, 'article_id', $id );
}

