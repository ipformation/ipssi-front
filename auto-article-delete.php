<?php
require_once('wp-load.php');

$pagesPublished = get_posts(
    array (
        'post_status' => 'publish',
        'post_type' => 'article'
    )
);

$pagesPending = get_posts(
    array (
        'post_status' => 'pending',
        'post_type' => 'article'
    )
);

if (!empty($pagesPublished)){
    foreach($pagesPublished as $page){
        $fields = get_post_custom($page->ID);
        $postID = $fields['article_id'][0];
        //Compare ID from GET
        if (isset($postID) && ($postID == $_GET['id'])){
            wp_delete_post( $page->ID, true );
        }
    }
}
if (!empty($pagesPending)){
    foreach($pagesPending as $page){
        $fields = get_post_custom($page->ID);
        $postID = $fields['article_id'][0];
        //Compare ID from GET
        if (isset($postID) && ($postID == $_GET['id'])){
            wp_delete_post( $page->ID, true );
        }
    }
}




