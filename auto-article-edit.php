<?php
require_once('wp-load.php');
$pagesPublished = get_posts(
    array (
        'post_status' => 'publish',
        'post_type' => 'article'
    )
);

$pagesPending = get_posts(
    array (
        'post_status' => 'pending',
        'post_type' => 'article'
    )
);

$id = $_GET['id'];
$url = 'http://projet-ipssi.com/app_dev.php/article/rest/article/'.$id;

/* gets the CVs from a URL */
$ch = curl_init();
$timeout = 5;
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$data = curl_exec($ch);
curl_close($ch);

$datadecode = json_decode($data);

$filename = $datadecode->webPath;

if ($datadecode->published){
    if (!empty($pagesPublished)){
        foreach($pagesPublished as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['article_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $article_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_type' => 'article',
                    'post_name' => $datadecode->title,
                    'post_status' => 'publish'
                );
                wp_insert_post($article_post);
                update_post_meta( $page->ID, 'description', $datadecode->description );
                update_post_meta( $page->ID, 'img_url', $datadecode->webPath );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    } elseif (!empty($pagesPending)) {
        foreach($pagesPending as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['article_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $article_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_type' => 'article',
                    'post_name' => $datadecode->title,
                    'post_status' => 'publish'
                );
                wp_insert_post($article_post);
                update_post_meta( $page->ID, 'description', $datadecode->description );
                update_post_meta( $page->ID, 'img_url', $datadecode->webPath );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    }
} else {
    if (!empty($pagesPending)){
        foreach($pagesPending as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['article_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $article_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_type' => 'article',
                    'post_name' => $datadecode->title,
                    'post_status' => 'pending'
                );
                wp_insert_post($article_post);
                update_post_meta( $page->ID, 'description', $datadecode->description );
                update_post_meta( $page->ID, 'img_url', $datadecode->webPath );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    } elseif (!empty($pagesPublished)) {
        foreach($pagesPublished as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['article_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $article_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_type' => 'article',
                    'post_name' => $datadecode->title,
                    'post_status' => 'pending'
                );
                wp_insert_post($article_post);
                update_post_meta( $page->ID, 'description', $datadecode->description );
                update_post_meta( $page->ID, 'img_url', $datadecode->webPath );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    }
}