<div class="wrap">

    <h2>Migration</h2>

    <? if ($migrationDone) :?>
        <div class="updated">
            <p>
                <?php echo __("Migration done", "webqam")?>
            </p>
        </div>
    <? endif ?>

    <form action="#" method="post">

        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="blogname"><?php echo __("Old URL", "webqam");?></label></th>
                    <td><input name="wp_em_oldurl" type="text" value="" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="blogname"><?php echo __("New URL", "webqam");?></label></th>
                    <td><input name="wp_em_newurl" type="text" value="<?php echo get_option("siteurl")?>" class="regular-text"></td>
                </tr>
            </tbody>
        </table>

        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php echo __("Do migration", "webqam")?>">
        </p>

    </form>

</div>