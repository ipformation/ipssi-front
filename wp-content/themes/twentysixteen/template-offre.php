<?php
/*
Template Name: Offre d'emploi
*/
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            //Méthode pour récupèrer l'id de l'offre pour l'injecter dans le formulaire car provient du backoffice et donc id différent
            //Récupère l'url en cours où se trouve l'id
            global $wp;
            $current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
            //On isole la fin de chaine avec l'id de l'offre
            $isolateEndString = strrchr($current_url, '/');
            //Offre en cours si 2 chiffres ok sinon, test si 3 chiffres ok TODO Prévoir à l'avenir les cas ou l'on a plus de 999 offres
            if (is_numeric(substr($isolateEndString, 1, 3))){
                $idOffreEnCours = (substr($isolateEndString, 1, 3));
            } else {
                $idOffreEnCours = (substr($isolateEndString, 1, 2));
            }

            // Start the loop.
            while ( have_posts() ) : the_post();

                // Include the page content template.
                get_template_part( 'template-parts/content', 'page' );
                // Intégration du formulaire de demande.
                echo do_shortcode( '[wordpress_file_upload fitmode="responsive" createpath="true" showtargetfolder="true" duplicatespolicy="maintain both" placements="title/userdata/filename+selectbutton/progressbar/message/uploadbutton" uploadtitle="Soumettre votre cv" selectbutton="Sélectionnez votre cv" uploadbutton="Répondre à cette offre" successmessage="Votre candidature a été envoyé avec succès" waitmessage="Votre CV %filename% est en cours d\'envoi" userdata="true" userdatalabel="Nom|t:text|s:left|r:1|a:0|p:bottom|d:/Prénom|t:text|s:left|r:1|a:0|p:right|d:/Email|t:email|s:left|r:1|a:0|v:1|p:right|d:|g:0/Métier|t:dropdown|s:left|r:0|a:1|p:right|d:|l:Développeur, Intégrateur, Graphiste/Offre|t:number|s:left|r:1|a:1|v:0|h:0|p:right|d:|f:right" medialink="true" postlink="true"]' );

                ?>
                <script>
                    // Ajout l'id dans le formulaire en champ caché + désactive la modification du champ
                    function AddIdOffer(){
                        var obj = document.getElementById('userdata_1_field_4');
                        obj.value = "<?php echo $idOffreEnCours; ?>";
                        obj.disabled=true;
                    }
                    AddIdOffer();
                    //Masque le champ dans le formulaire
                    function hideThis(){
                        var obj = document.getElementById('userdata_1_4');
                        obj.style.display = "none";
                    }
                    hideThis();
                </script>
                <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }

                // End of the loop.
            endwhile;
            ?>

        </main><!-- .site-main -->

        <?php get_sidebar( 'content-bottom' ); ?>

    </div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>