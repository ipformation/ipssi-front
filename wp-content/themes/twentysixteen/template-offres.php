<?php
/*
Template Name: Offres d'emploi
*/
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();

                // Include the page content template.
                get_template_part( 'template-parts/content', 'page' );

                $parentid = $post->ID;
                    $children = wp_list_pages( 'title_li=&child_of='.$post->ID.'&echo=0&sort_order=desc' );
                    if ( $children) : ?>
                        <?php echo '<h2>' . $children . '</h2>'; ?>
                    <?php endif; ?>
                <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }

                // End of the loop.
            endwhile;
            ?>

        </main><!-- .site-main -->

        <?php get_sidebar( 'content-bottom' ); ?>

    </div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>