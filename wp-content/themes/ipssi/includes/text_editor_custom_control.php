<?php
if (class_exists('WP_Customize_Control'))
{
    /**
     * Class to create a custom tags control
     */
    class Text_Editor_Custom_Control extends WP_Customize_Control
    {
        /**
         * Render the content on the theme customizer page
         */
        public function render_content()
        {
            ?>
            <label>
                <span class="customize-text_editor"><?php echo esc_html( $this->label ); ?></span>
                <input type="hidden" <?php $this->link(); ?> value="<?php echo esc_textarea( $this->value() ); ?>">
                <?php
                $settings = array(
                    'textarea_name' => $this->id,
                    'media_buttons' => false,
                    'drag_drop_upload' => false,
                    'teeny' => true
                );
                wp_editor($this->value(), $this->id, $settings );

                do_action('admin_footer');
                do_action('admin_print_footer_scripts');

                ?>
            </label>
            <?php
        }
    }
}


function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/includes/assets/customizer-panel.js', array( 'jquery' ), rand(), true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

?>