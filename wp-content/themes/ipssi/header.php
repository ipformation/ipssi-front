<!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php echo (wp_is_mobile()) ? 'is-mobile' : 'is-desktop'?>">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php echo Webqam::getInstance()->getTemplatePartWithVars("partials/analytics.php",
        array("tag" => get_option("ua_tracking_code")))?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOcpz9a4QKPG69IM0AR8VElQjAFnZYPTE"></script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

    <header class="site-header" role="banner">
        <div class="top-header">
            <div class="grid grid-pad">
                <?php
                    if (get_option("facebook_url") !== false) {
                        ?>
                        <a target="_blank" href="<?php echo get_option("facebook_url") ?>" class="top-header-social top-header-facebook">
                            <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-facebook.svg'?>" width="16" height="16" alt="Facebook">
                        </a>
                        <?php
                    }
                ?>
                <?php
                if (get_option("twitter_url") !== false) {
                    ?>
                    <a target="_blank" href="<?php echo get_option("twitter_url") ?>" class="top-header-social top-header-twitter">
                        <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-twitter.svg'?>" width="16" height="16" alt="Twitter">
                    </a>
                    <?php
                }
                ?>
                <?php
                if (get_option("pinterest_url") !== false) {
                    ?>
                    <a target="_blank" href="<?php echo get_option("pinterest_url") ?>" class="top-header-social top-header-pinterest">
                        <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-pinterest.svg'?>" width="16" height="16" alt="Pinterest">
                    </a>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="main-header grid">
            <div class="col-1-12 mobile-col-3-12">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site-logo">
                    <img src="<?php echo get_theme_mod("logo")?>" alt="<?php bloginfo("name") ?>" />
                </a>
            </div>
            <div class="col-11-12 mobile-col-9-12">
                <nav class="site-navigation primary-navigation" role="navigation">
                    <i class="site-navigation-toggle"></i>
                    <?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'menu_class' => 'nav-menu' ) ); ?>
                </nav>
            </div>
        </div>
    </header>
