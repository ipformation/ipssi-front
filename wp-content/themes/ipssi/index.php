<?php get_header();
$pageId = get_option( 'page_for_posts' );
$fields = get_post_custom($pageId);
$limit = $fields['post_limit'][0];
$categories = get_categories();

?>

<div id="main-content" class="main-content">
    <div class="main-content blog">
        <section class="blog-banner">
            <?php echo get_the_post_thumbnail($pageId) ?>
            <div class="blog-banner-content-wrapper">
                <div class="blog-banner-content grid">
                    <h1 class="main-title">
                        <?php echo get_the_title($pageId) ?>
                    </h1>
                    <ul class="blog-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="blog-content">
            <div class="blog-content-post-list">
                <div class="grid" id="postGrid" data-limit="<?php echo $limit ?>">
                    <?php
                    $i = 0;
                    query_posts(array('post_type' => 'article','orderby' => 'menu_order', 'posts_per_page' => $limit)); if(have_posts()) : while(have_posts()) : the_post();
                        include(locate_template('content/content-post.php'));
                    $i++;
                    endwhile; endif; wp_reset_query(); ?>
                    <div class="grid-loader">
                        <img src="<?php echo get_template_directory_uri().'/assets/images/loader.png'?>" alt="">
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<?php
get_footer();
