// Declare namespace
var WQ = WQ || {};

WQ.General = function() {
};

WQ.General.prototype = {
    init: function() {
        var that = this;
        that.headerLink();
        that.fillSvg();
        that.mobileNav();
        that.contactForm();
        that.fixedCta();
        that.campusBachelor();
        that.showGoogleMaps();
        that.blogPage();
        that.jpoPage();
        that.workPage();
        that.documentationPage();
    },
    onLoad: function() {
        var that = this;
        that.homeSlider();
        that.eventSlider();
        that.bachelorSlider();
        that.stepSlider();
        that.bachelorPage();
        that.campusPage();
        that.testimonySlider();

    },
    documentationPage: function (){
        var $radio = $('.wpcf7-radio input[type="radio"]');

        $('.wpcf7-list-item-label').on('click', function() {
            $radio.prop( "checked", false );
            $(this).parent().find($radio).prop( "checked", true );
        });
    },
    jpoPage: function() {
        var $item = $('.jpo-content-list-item');
        var $input = $('input.hidden');
        $item.on('click', function(){
            var campus = $(this).attr('data-jpo');
            $item.removeClass('selected');
            $input.val(campus);
            $(this).toggleClass('selected');
        });
    },
    workPage: function() {
        $('.work-taxonomy-item').on('click', function () {
            $(this).find('.work-taxonomy-item-work-list').parent().toggleClass('visible');
        });
    },
    blogPage: function() {
        var that = this;
        var $window = $(window);
        var $grid = $('#postGrid');
        var limit = parseInt($grid.attr('data-limit'));
        var cat = $('.blog-content-filter-campus').val();
        var campus = $('.blog-content-filter-category').val();
        that.ready = true;

        // FILTERS
        $('select').selectric({
            onChange: function(element) {
                if (that.ready) {
                    campus = $('.blog-content-filter-campus').val();
                    cat = $('.blog-content-filter-category').val();
                    $grid.find('.blog-content-post-item').remove();
                    that.doAjaxRequest(0, limit, cat, campus );
                }
            }
        });

        //INFINITE
        $window.on('scroll', function () {
            if($window.scrollTop() + $window.height() == $(document).height() && that.ready) {
                var offset = $('#postGrid .blog-content-post-item').slice(-1).attr('data-offset');
                that.doAjaxRequest(offset, limit, cat, campus );
            }
        });
    },
    doAjaxRequest: function(offset, limit, category, campus) {
        var that = $(this);
        that.ready = false;

        var ajaxurl = '/wp-admin/admin-ajax.php';
        var $grid = $('#postGrid');
        $grid.addClass('loading');

        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                'action': 'do_ajax',
                'fn': 'getPost',
                'offset': offset,
                'limit': limit,
                'category': category,
                'campus': campus
            },
            success: function (response) {
                $.when (
                    $grid.imagesLoaded(function () {
                        $grid.append($(response));
                    })
                ).then(function(){
                    setTimeout(function(){
                        $grid.removeClass('loading');
                        $('#postGrid .blog-content-post-item').slice(-+limit).addClass("visible");
                        that.ready = true;
                    }, 150);
                });
            },
            error: function() {
                console.log('error');
            }
        });
    },
    showGoogleMaps: function(){


        if ($('.main-contact').hasClass('has-map')) {
            var $container = $('.main-contact-gmap');
            var lat = parseFloat($container.attr('data-lat'));
            var lng = parseFloat($container.attr('data-lng'));
            var icon = $container.attr('data-icon');
            var contentString = $container.attr('data-content');

            var myLatLng = {lat: lat, lng: lng};

            var map = new google.maps.Map(document.getElementById('isefacMap'), {
                zoom: 17,
                center: myLatLng,
                disableDefaultUI: true
            });

            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                pixelOffset: new google.maps.Size(-50,250)
            });


            var marker = new google.maps.Marker({
                position: map.getCenter(),
                icon: icon,
                map: map
            });

            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });

            google.maps.event.trigger(marker, 'click');

            google.maps.event.addListener(infowindow, 'domready', function() {

                // Reference to the DIV which receives the contents of the infowindow using jQuery
                var iwOuter = $('.gm-style-iw');

                var iwBackground = iwOuter.prev();

                iwBackground.css({'display' : 'none'});

                var iwCloseBtn = iwOuter.next();

                iwCloseBtn.css({
                    opacity: '0'
                });
            });

        }

    },
    campusBachelor: function () {
        var $item = $('.campus-bachelor-list-item-content-item');

        $item.on('mouseenter', function() {
            var $this = $(this);

            if ($this.hasClass('active') === false) {
                var classes = $this.attr('class').split(/\s+/);
                console.log('.campus-bachelor-list-item-image.'+classes[1]);
                $this.parent().find($item).removeClass('active');
                $this.addClass('active');
                $this.parent().parent().parent().find('.campus-bachelor-list-item-image').removeClass('active');
                $this.parent().parent().parent().find('.campus-bachelor-list-item-image.'+classes[1]).addClass('active');
            }
        });

    },
    testimonySlider: function () {
        $('.bachelor-testimony-list').bxSlider({
            adaptiveHeight: true,
            mode: 'fade',
            auto: true
        });
    },
    campusPage: function (){


        var currentScroll = 0;

        var $aside     = $(".campus-content-aside-list"),
            asideH     = $aside.outerHeight(),
            $window    = $(window),
            offset     = $('.campus-banner').outerHeight();

        $window.scroll(function() {

            var maxHeight = $('.campus-content').outerHeight();
            if ($window.scrollTop() > offset && $window.scrollTop() < maxHeight) {
                $aside.css('padding-top', $window.scrollTop() - asideH - 50);
                currentScroll = $window.scrollTop();
            }
            else {
                $aside.css('padding-top', currentScroll-asideH-50);
            }
        });

        $('.campus-content-header').on('click', function() {
            var $parent = $(this).parent();
            $parent.toggleClass('active');
            $window.scroll();
        });

        $window.on('resize', function () {
            $window.scroll();
        });

    },
    bachelorPage: function (){
        var currentScroll = 0;

        var $aside     = $(".bachelor-content-aside-list"),
            asideH     = $aside.outerHeight(),
            $window    = $(window),
            offset     = $('bachelor-banner').outerHeight();

        $window.scroll(function() {
            var maxHeight = $('.bachelor-content').outerHeight();
            if ($window.scrollTop() > offset && $window.scrollTop() < maxHeight) {
                $aside.css('padding-top', $window.scrollTop() - asideH - 50);
                currentScroll = $window.scrollTop();
            }
            else {
                $aside.css('padding-top', currentScroll-asideH-50);
            }
        });

        $('.bachelor-content-header').on('click', function() {
            var $parent = $(this).parent();
            $parent.toggleClass('active');
            $window.scroll();
        });

        $window.on('resize', function () {
            $window.scroll();
        });

    },
    stepSlider: function() {
        var contentHeight = $('.school-steps-list-item.active').find('.school-steps-list-item-content').outerHeight();
        var $container = $('.school-steps-list');
        var $navItem = $('.school-steps-list-item');

        $container.css('padding-bottom', contentHeight);

        $navItem.on('click', function() {
            $navItem.removeClass('active');
            contentHeight = $(this).find('.school-steps-list-item-content').outerHeight();
            $container.css('padding-bottom', contentHeight);
            $(this).toggleClass('active');
        });

    },
    fillSvg: function () {
        $('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }


                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
                if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
                }

                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
    },
    fixedCta: function() {

        var $aside = $('.aside-cta');
        var distance = $('header').height() + $('.home-main-slider').height();

        $(window).on('scroll', function() {
            var distanceY = window.pageYOffset || document.documentElement.scrollTop;
            if (distanceY > distance) {
                $aside.removeClass('hidden');
            } else {
                $aside.addClass('hidden');
            }
        });

    },
    headerLink: function() {

        var isMobile = $('html').hasClass('is-mobile');
        var $hasChildren = $('.menu-item-has-children');
        var $items = $('#menu-header > li');

        if (isMobile) {
            $hasChildren.on('click', function() {
                $(this).toggleClass('active');
            });
        }
        else {
            var hoverTimeout;

            $items.on('mouseenter', function(){
                clearTimeout(hoverTimeout);
                $items.removeClass('active');
                $(this).addClass('active');
            }).on('mouseleave', function(){
                var $this=$(this);
                hoverTimeout = setTimeout(function() {
                    $this.removeClass('active');
                },250);
            });
        }

    },
    homeSlider: function () {
        $('.home-slider').bxSlider({
            adaptiveHeight: true,
            mode: 'fade'
        });
    },
    mobileNav: function () {
        $('.site-navigation-toggle').on('click', function (){
           $('.nav-menu').toggleClass('visible');
        });
    },
    bachelorSlider: function () {
        var width = $(window).width();
        var itemW = 0;
        var items = 0;
        if (width >= 1024) {
            itemW = 300;
            items = Math.floor(width / itemW);
            if (items >= 3) {
                items = 3;
            }
        }
        else if (width <= 1024 && width > 480) {
            itemW = 290;
            items = Math.round(width / itemW);
            if (items >= 3) {
                items = 3;
            }
        }
        else {
            itemW = 290;
            items = 1;
        }

        $('.home-bachelor-slider').bxSlider({
            minSlides: 1,
            maxSlides: items,
            slideWidth: itemW,
            slideMargin: 20,
            moveSlides: 1,
            adaptiveHeight: false
        });
    },
    eventSlider: function () {

        var width = $(window).width();
        var itemW = 290;
        var items = Math.floor(width / itemW);

        $('.home-events-slider').bxSlider({
            minSlides: 1,
            maxSlides: items,
            slideWidth: itemW,
            slideMargin: 10,
            moveSlides: 1,
            adaptiveHeight: false
        });
    },
    contactForm: function() {
        var $form = $('.wpcf7-form');
        var $formSubmit = $('.wpcf7-submit');

        $('.wpcf7-form-control').on('change', function() {
            var that = $(this);
            var val = that.val();
            if (val !== "") {
               that.parent().parent().addClass('is-filled');
            }
            else {
                that.parent().parent().removeClass('is-filled');
            }
        });

        $form.on('submit', function (e) {
            $formSubmit.addClass('loading');
            $.when($form.is('.invalid, .failed')).done(function() {
                setTimeout(function(){
                    $formSubmit.removeClass('loading');
                }, 500);
            });
        });

    }

};

$(document).ready(function() {

    var g = new WQ.General();
    g.init();

});

$(window).load(function() {
    var g = new WQ.General();
    g.onLoad();
});

