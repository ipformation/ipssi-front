<?php
require_once realpath(__DIR__ . '/includes/class/Singleton.php');
require_once realpath(__DIR__ . '/includes/helpers.php');
require_once realpath(__DIR__ . '/includes/options.php');
require_once realpath(__DIR__ . '/includes/text_editor_custom_control.php');

/**
 * Webqam singleton.
 *
 * @author Olivier Barou <obarou@ipssi.fr>
 */
class Webqam extends Singleton {

    const PACKAGE_VERSION = '0.0.1';

    /**
     * @var int
     */
    protected $cacheVersion;

    /**
     * Constructor.
     */
    protected function __construct() {

        $this->cacheVersion = LOCAL_WORK || IS_PREPRODUCTION ? time() : self::PACKAGE_VERSION;
        $this->tplDir = get_template_directory_uri();

    }

    /**
     * @return Webqam
     *
     * Overrided to get autocompletion in IDE, nothing else to do here
     */
    public static function getInstance() {
        return parent::getInstance();
    }

    /**
     * Initialization.
     */
    public function init() {

        load_theme_textdomain( 'ipssi', get_template_directory() . '/languages' );

        $this->actions();
        $this->filters();
        $this->menus();
        $this->images();
        $this->defines();
        $this->singleTemplate();
        $this->initCustomizations();
    }

    /**
     * Add the custom field for the theme
     */
    protected function initCustomizations() {

        require_once realpath(__DIR__ . '/includes/class/Webqam_customize.php');
        // Setup the Theme Customizer settings and controls...
        add_action( 'customize_register' , array( 'Webqam_Customize' , 'register' ) );

    }

    /**
     * @return $this
     */
    protected function actions() {

        if (!is_admin()) {
            add_action('wp_enqueue_scripts', array($this, 'loadStyles'));
            add_action('wp_enqueue_scripts', array($this, 'loadScripts'));
        }

        // Add AJAX actions
        add_action('wp_ajax_nopriv_do_ajax', array($this, 'getPost'));
        add_action('wp_ajax_do_ajax', array($this, 'getPost'));

        // Image crop
        add_image_size( 'home-article', 290, 224, true );
        add_image_size( 'blog-article', 460, 264, true );

        //  Remove Wordpress meta informations from header
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        //  Remove extra feed links from header
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'feed_links_extra', 3);

        // Adds a noindex meta tag on preprod, Google seems to sometimes
        // ignore robots.txt instructions.
        add_action('wp_head', function() {

            if (IS_PREPRODUCTION)
                echo '<meta name="robots" content="noindex,follow" />';

        });

        return $this;

    }

    protected function filters() {

        /**
         * Adds "start_in" argument.
         *
         * only submenus of this id will be displayed.
         */
        add_filter("wp_nav_menu_objects", function($sortedMenuItems, $args) {

            if(!isset($args->start_in))
                return $sortedMenuItems;

            $objectByitems = array();

            // In parent mode, start at current parent menu item
            if ($args->start_in == "parent") {
                foreach( $sortedMenuItems as $item ) {

                    $objectByitems[$item->ID] = $item->object_id;

                    // If it's curent item
                    if ($item->current)
                        $args->start_in =  $item->menu_item_parent;

                }

                // If there is no "current"
                if ($args->start_in == "parent")
                    return null;

                $args->start_in = $objectByitems[$args->start_in];
            }


            $menuItemParents = array();
            foreach( $sortedMenuItems as $key => $item ) {
                // init menu_item_parents
                if( $item->object_id == (int)$args->start_in )
                    $menuItemParents[] = $item->ID;

                if( in_array($item->menu_item_parent, $menuItemParents) ) {
                    // part of sub-tree: keep!
                    $menuItemParents[] = $item->ID;
                } else {
                    // not part of sub-tree: away with it!
                    unset($sortedMenuItems[$key]);
                }
            }

            return $sortedMenuItems;

        },10,2);


        // These 2 hooks removes domain on images src when they are uploaded/added to an article.
        // This makes transfers/production/domain changes a lot easier.
        add_filter('wp_handle_upload', function($file){

            // removes absolute prefix from URL
            $file["url"] = str_replace(get_bloginfo('url'), "", $file["url"]);

            return $file;
        });
        add_filter('image_send_to_editor', function($html){

            // removes absolute prefix from image src and link
            $html = str_replace(get_bloginfo('url'), "", $html);

            return $html;
        });

    }

    /**
     * Register basic menus.
     *
     * @return $this
     */
    protected function menus() {
        add_theme_support('menus');
        register_nav_menu('main-nav', 'Navigation principale');
        register_nav_menu('footer-nav-subscribe', 'Footer Inscription');
        register_nav_menu('footer-nav-school', 'Footer Ecole');
        register_nav_menu('footer-nav-contact', 'Footer Contact');

        return $this;
    }

    /**
     * Specifies thumbnail sizes.
     *
     * @return $this
     */
    protected function images() {
        //  Enable thumbnails
        add_theme_support('post-thumbnails');

        //  Specify image formats
        // add_image_size("format", 720, 250);

        return $this;

    }

    /**
     * Define here all constants you need.
     *
     * @return $this
     */
    protected function defines() {

        // Define directory for single page
        defined('SINGLE_PATH') || define('SINGLE_PATH', TEMPLATEPATH . '/single-templates');

        return $this;
    }

    /**
     * Define directory for single page
     *
     * @return void
     */
    protected function singleTemplate() {

        add_action( 'single_template', function($single_template) {
            global $post;

            if(file_exists(SINGLE_PATH . '/single-' . $post->post_type . '.php'))
                $single_template = SINGLE_PATH . '/single-' . $post->post_type . '.php';

            return $single_template;

        } );

    }

    /**
     * Register and enqueue scripts.
     *
     * @return $this
     */
    public function loadScripts() {
        //  Register
        wp_register_script('plugins', "$this->tplDir/assets/js/build/vendors.min.js", array(), $this->cacheVersion, true);
        wp_register_script('scripts', "$this->tplDir/assets/js/build/scripts.min.js", array("plugins"), $this->cacheVersion, true);

        //  Enqueue
        wp_enqueue_script('plugins');
        wp_enqueue_script('scripts');

        return $this;

    }

    /**
     * Register and enqueue styles.
     *
     * @return $this
     */
    public function loadStyles() {

        //  Register
        wp_register_style('main', "$this->tplDir/assets/css/front.min.css", array(), $this->cacheVersion);

        //  Enqueue
        wp_enqueue_style('main');

        return $this;

    }

    /**
     * Return a template with the given vars previously declared.
     *
     * @param string $template
     * @param array $vars
     * @return string
     */
    public function getTemplatePartWithVars($template, $vars = array()) {

        if (!empty($vars))
            extract($vars);

        ob_start();
        include(locate_template($template));
        return ob_get_clean();
    }


    /**
     * Pagination
     *
     * @return $this
     */
    public function getPagination() {

        if( is_singular() )
            return;

        global $wp_query;

        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
            return;

        get_template_part('content/content', 'pagination');

        return $this;

    }

    /**
     * Return last posts
     *
     * @return array
     */
    public function getPost()
    {

        $category = filter_var($_POST['category'], FILTER_SANITIZE_NUMBER_INT);
        $campus = $_POST['campus'];
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        preg_match_all('!\d+!', $category, $cat);

        if ( $category == "all" ) {
            $args = array(
                'post_type' => 'post',
                'offset' => $offset,
                'posts_per_page ' => -1
            );
        }
        else {
            $args = array(
                'post_type' => 'post',
                'offset' => $offset,
                'cat' => $cat[0][0],
                'posts_per_page ' => -1
            );
        }

        $posts = new WP_Query($args);
        $i = 0;
        $ii = $offset;
        foreach ( $posts->posts as $post )  {
            $ii++;
            $fields = get_post_custom($post->ID);
            $postCampus = $fields['campus'][0];
            if ( $campus == 'all' || $campus == 'campus-'.$postCampus ) {
                include(locate_template("content/content-post-ajax.php"));
                $i++;
                if ($i == $limit) {
                    break;
                }
            }
        }
    }

}

add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
    return  get_bloginfo('stylesheet_directory') . '/assets/images/loader.png';
}

Webqam::getInstance()->init();