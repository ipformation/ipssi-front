<?php
/**
 * The template for Cursus
 */
global $cursus;
$cursusFields = get_post_custom($cursus->ID);
$duration = $cursusFields['duration'][0];
$nbDuration = (int)filter_var($duration, FILTER_SANITIZE_NUMBER_INT);
$isLinked = $cursusFields['is_linked'][0];

?>

<li class="cursus-section-list-item <?php echo  ($isLinked !== 'null') ? 'is-linked' : ''?>">
    <div class="cursus-section-list-item-inner">
        <div class="cursus-section-list-item-header">
            <h3 class="third-title">
                <?php echo  $cursus->post_title?>
            </h3>
            <p class="cursus-section-list-item-option">
                <?php echo  $cursusFields['option'][0] ?>
            </p>
        </div>

        <div class="cursus-section-list-item-duration">
            <?php
            for ($i = 0; $i < 3; $i++) {
                ?>
                <i class="icon icon-calendar <?php echo  ($nbDuration > 0) ? 'active': '' ?>"></i>
                <?php
                $nbDuration--;
            }
            ?>
            <p>
                <?php echo  $duration ?>
            </p>
        </div>
        <div class="cursus-section-list-item-stage">
            <p>
                <?php echo  $cursusFields['stage_duration'][0]?>
            </p>
            <a href="<?php the_permalink($cursus->ID)?>" class="button round"> + </a>
        </div>
    </div>
</li>

<?php
    if ($isLinked !== "null") {
        $cursusFields = get_post_custom($isLinked);
        $duration = $cursusFields['duration'][0];
        $nbDuration = (int)filter_var($duration, FILTER_SANITIZE_NUMBER_INT);
        ?>
        <li class="cursus-section-list-item is-linked">
            <div class="cursus-section-list-item-inner">
                <div class="cursus-section-list-item-header">
                    <h3 class="third-title">
                        <?php echo  get_the_title($isLinked) ?>
                    </h3>
                    <p class="cursus-section-list-item-option">
                        <?php echo  $cursusFields['option'][0] ?>
                    </p>
                </div>

                <div class="cursus-section-list-item-duration">
                    <?php
                    for ($i = 0; $i < 3; $i++) {
                        ?>
                        <i class="icon icon-calendar <?php echo  ($nbDuration > 0) ? 'active': '' ?>"></i>
                        <?php
                        $nbDuration--;
                    }
                    ?>
                    <p>
                        <?php echo  $duration ?>
                    </p>
                </div>
                <div class="cursus-section-list-item-stage">
                    <p>
                        <?php echo  $cursusFields['stage_duration'][0]?>
                    </p>
                    <a href="<?php echo  get_permalink($isLinked)?>" class="button round"> + </a>
                </div>
            </div>
        </li>
        <?php
    }
?>