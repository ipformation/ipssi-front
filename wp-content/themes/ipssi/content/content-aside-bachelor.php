<?php
/**
 * The template for displaying Bachelors aside
 */
$name = get_field('name');
?>
<li class="bachelor-content-aside-list-item <?php echo ($name == $title) ? 'active' : '' ?>">
    <h2 class="second-title">
        <a class="bachelor-content-aside-list-item-link" href="<?php echo get_post_permalink() ?>" title="<?php echo $name ?>">
            <?php echo $name ?>
        </a>
    </h2>
</li>