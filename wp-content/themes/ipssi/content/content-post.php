<?php
/**
 * The template for displaying Posts
 */
?>

<div class="blog-content-post-item all visible col-1-2"
            data-offset="<?php echo $limit ?>">
    <div class="blog-content-post-item-wrapper">
        <a class="blog-content-post-link" href="<?php echo get_the_permalink($post->ID) ?>" title="<?php echo the_title() ?>">
            <?php the_post_thumbnail('blog-article') ?>
            <div class="blog-content-post-item-content">
                <h2 class="second-title">
                    <?php echo the_title() ?>
                </h2>
                <p class="blog-content-post-item-content-date">
                    <?php _e('Le', 'webqam') ?> <?php echo get_the_date( 'd F Y', $post->ID ); ?>
                </p>
                <div class="blog-content-post-item-content-tags">
                    <?php
                    echo strip_tags(get_the_tag_list('<ul><li>','</li><li>','</li></ul>'), '<ul><li>');
                    ?>
                </div>
            </div>
        </a>
    </div>
</div>
