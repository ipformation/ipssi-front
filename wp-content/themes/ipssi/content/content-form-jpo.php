<?php
/**
 * The template for displaying JPO
 */

$campus = get_field('place');
$JPOs = (array)get_field('jpo');

?>

<?php
    if ($JPOs[0]) {
        ?>
        <li class="jpo-content-list-item <?php echo ($i%4 == 0) ? 'last' : ''?>" data-jpo="<?php echo $campus ?>">
            <div class="jpo-content-list-item-wrapper">
                <h3 class="third-title">
                    <?php echo $campus ?>
                </h3>
                <?php
                foreach ($JPOs as $jpo) {
                    $jpoField = get_post_custom($jpo->ID);
                    $date = $jpoField['date'][0];
                    $date = \DateTime::createFromFormat('d/m/Y', $date);
                    $date = date_i18n("d F", strtotime($date->format('Y-m-d')));
                    $hours = $jpoField['hours'][0];
                    ?>
                    <p class="jpo-content-list-item-date">
                        <?php echo  $date ?>
                        <span>
                        <?php echo  $hours ?>
                    </span>
                    </p>
                    <?php
                }
                ?>
            </div>
        </li>
        <?php
    }
?>
