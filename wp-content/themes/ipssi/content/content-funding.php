<?php
/**
 * The template for displaying Bachelors
 */

$title = get_field('title');
$icon = get_field('icon');
$content = get_field('content');
?>

<li class="rate-funding-list-item">
    <div class="rate-funding-list-item-header">
        <i class="icon icon-<?php echo $icon?>"></i>
        <h3 class="third-title">
            <?php echo  $title ?>
        </h3>
    </div>
    <div class="rate-funding-list-item-content">
        <?php echo  $content ?>
    </div>
</li>
