<?php
/**
 * The template for displaying Campus aside
 */
$name = get_field('place');
?>
<li class="campus-content-aside-list-item <?php echo ($name == $title) ? 'active' : '' ?>">
    <h4 class="fourth-title">
        <a class="campus-content-aside-list-item-link" href="<?php echo get_post_permalink() ?>" title="<?php echo $name ?>">
            <?php echo $name ?>
        </a>
    </h4>
</li>