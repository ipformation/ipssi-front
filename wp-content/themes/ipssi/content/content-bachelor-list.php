<?php
/**
 * The template for displaying Bachelor type
 */


$title = $taxonomy->name;
?>
    <?php
        if ($i !== 0 && $i%3 == 0) {
    ?>
    <span class="bachelor-list-sep"> - </span>
</ul>
<ul class="home-slider-bachelor-list">
    <?php
    }
    elseif ($i !== 0) {
        ?>
        -
        <?php
    }
    ?>
    <li class="home-slider-bachelor-item">
        <?php echo  $title ?>
    </li>
