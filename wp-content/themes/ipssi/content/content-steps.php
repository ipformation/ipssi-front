<?php
/**
 * The template for displaying Steps
 */
$title = get_field('title') ;
$content = get_field('content');

?>

<li class="school-steps-list-item <?php echo ($i == 1) ? 'active' : ''; ?>">
    <div class="school-steps-list-item-header">
        <span class="item-header-count">
            <?php echo  $i ?>
        </span>
        <h3 class="third-title">
            <?php echo  $title ?>
        </h3>
    </div>
    <div class="school-steps-list-item-content">
        <h3 class="third-title mobile-only">
            <?php echo  $title ?>
        </h3>
        <?php echo  $content ?>
    </div>
</li>