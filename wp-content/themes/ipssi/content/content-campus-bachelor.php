<?php
/**
 * The template for displaying bachelors in Campus
 */
$args= array(
    'post_type' => 'bachelor',
    'tax_query' => array(
        array(
            'taxonomy' => 'cpt_type',
            'field'    => 'slug',
            'terms'    => array( $taxonomy->slug ),
        ),
    ),
);
$thisTypeBachelor = new WP_Query($args);

$hasCampus = false;

?>
<li class="campus-bachelor-list-item col-1-2 <?php echo ($i%2 == 0) ? 'first' : 'second'?>">
    <div class="campus-bachelor-list-item-wrapper">
        <?php
        $ii = 0;
        foreach ( $thisTypeBachelor->posts as $bachelor ) {
            $fields = get_post_custom($bachelor->ID);
            $img = wp_get_attachment_image($fields['image'][0], 'big');
            ?>
            <div class="campus-bachelor-list-item-image is-<?php echo $bachelor->ID ?> <?php echo ($ii == 0) ? 'active' : ''?>">
                <?php
                echo $img;
                ?>
            </div>

            <?php
            $ii++;
        }

        ?>

        <div class="campus-bachelor-list-item-content">
            <h3 class="third-title">
                <?php the_field('shorthand', $taxonomy); ?>
            </h3>
            <ul class="campus-bachelor-list-item-content-list">
                <?php
                $iii = 0;
                foreach ( $thisTypeBachelor->posts as $bachelor ) {
                    $fields = get_post_custom($bachelor->ID);
                    $link = get_post_permalink($bachelor->ID);
                    $name = $fields['name'][0];
                    ?>
                    <li class="campus-bachelor-list-item-content-item is-<?php echo $bachelor->ID ?> <?php echo ($iii == 0) ? 'active' : ''?>">
                        <a class="campus-bachelor-list-item-content-item-link" href="<?php echo $link; ?>">
                            <?php
                            echo $name;
                            ?>
                        </a>
                    </li>
                    <?php
                    $iii++;
                }
                ?>
            </ul>
        </div>
    </div>
</li>