<?php
/**
 * The template for displaying Bachelors
 */

$date = the_date('Y-m-d')
?>

<li class="home-events-slide">
    <div class="home-events-slide-wrapper">
        <div class="home-slider-events-slide-thumb">
            <?php the_post_thumbnail('home-article') ?>
        </div>
        <div class="home-events-slide-content">
            <h4 class="fourth-title">
                <?php echo  the_title() ?>
            </h4>
            <div class="home-events-slide-date">
                <? _e('Le') ?>
                <?php echo  date_i18n( get_option( 'date_format' ), strtotime( $date ) ); ?>
            </div>
            <div class="home-events-slide-tags">
                <?php
                echo strip_tags(get_the_tag_list('<ul><li>','</li><li>','</li></ul>'), '<ul><li>');
                ?>
            </div>
        </div>

    </div>
</li>