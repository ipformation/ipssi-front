<?php
/**
 * The template for Work Taxonomies
 */

$args= array(
    'post_type' => 'work',
    'tax_query' => array(
        array(
            'taxonomy' => 'work_type',
            'field'    => 'slug',
            'terms'    => array( $taxonomy->slug ),
        ),
    ),
);
$thisTypeWork = new WP_Query($args);
$count = count($thisTypeWork->posts);
?>

<li class="work-taxonomy-item">
    <div class="work-taxonomy-item-header">
        <h2 class="second-title">
            <?php echo $taxonomy->description ?>
        </h2>
    </div>
    <ul class="work-taxonomy-item-work-list">
        <?php
            $i = 0;
            foreach ($thisTypeWork->posts as $work) {
                ?>
                <li class="work-taxonomy-item-work-item <?php echo ($i%2 == 0) ? 'first' : 'last' ?>">
                    <div class="work-taxonomy-item-work-item-wrapper">
                        <h3 class="third-title">
                            <?php echo $work->post_title ?>
                        </h3>
                        <p class="work-taxonomy-item-work-item-content">
                            <?php echo $work->post_content ?>
                        </p>
                        <a href="<?php the_permalink($work->ID)?>" class="button orange round">
                            +
                        </a>
                    </div>
                </li>
                <?php
                $i++;
            }
        ?>
        <?php
        $description = get_field('description', $taxonomy);
        if ($i == $count && $description) {
            ?>
            <div class="work-taxonomy-item-work-description">
                <?php echo $description ;?>
            </div>
            <?php
        }
        ?>
    </ul>
</li>