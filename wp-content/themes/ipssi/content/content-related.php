<?php
/**
 * The template for displaying Related posts
 */
?>
<li class="post-related-item">
    <div class="post-related-item-wrapper">
        <a class="post-related-item-link" href="<?php echo get_the_permalink() ?>" title="<?php echo the_title() ?>">
            <?php the_post_thumbnail('home-article') ?>
            <div class="post-related-item-content">
                <h3 class="third-title">
                    <?php echo the_title() ?>
                </h3>
                <p class="post-related-item-content-date">
                    <?php _e('Le', 'webqam') ?> <?php echo get_the_date( 'd F Y', $post->ID ); ?>
                </p>
                <div class="post-related-item-content-tags">
                    <?php
                    echo strip_tags(get_the_tag_list('<ul><li>','</li><li>','</li></ul>'), '<ul><li>');
                    ?>
                </div>
            </div>
        </a>
    </div>
</li>
