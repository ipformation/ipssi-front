<?php
/**
 * The template for displaying Bachelors
 */

?>


<section class="home-zoom-article" style="background-image: url('<?php the_post_thumbnail_url( 'large' ) ?>')">
    <div class="home-zoom-article-wrapper">
        <div class="grid">
            <div class="col-1-2">
                <h3 class="third-title">
                    <?php echo $zoomTitle ?>
                </h3>
                <div class="home-zoom-article-item">
                    <div class="home-zoom-article-wrapper">
                        <div class="home-zoom-article-thumbnail">
                            <?php the_post_thumbnail('medium-large') ?>
                        </div>
                        <div class="home-zoom-article-content">
                            <h5 class="fifth-title">
                                <?php echo the_title() ?>
                            </h5>
                            <p>
                                <?php echo get_the_excerpt() ?>
                            </p>
                            <a href="<?php echo get_the_permalink() ?>" class="button white">
                                <?php _e('Découvrir'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>