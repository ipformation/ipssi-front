<?php
/**
 * The content for application list
 */
global $post;

?>

<div class="application-item col-1-3">
    <a href="<?php the_permalink($post->ID) ?>" class="application-item-link">
        <img src="/wp-content/uploads/2016/08/img-home-jpo.png" alt="">
        <h2 class="second-title">
            <?php
            echo $post->post_title;
            ?>
        </h2>
        <p class="application-content">
            <?php
            echo $post->post_content;
            ?>
        </p>
    </a>
</div>

