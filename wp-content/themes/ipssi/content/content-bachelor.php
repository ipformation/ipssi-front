<?php
/**
 * The template for displaying Bachelors
 */
$name = get_field('name');
$wording = get_field('button');
$thumbnail = get_field('image');
$isBachelor = (bool)get_field('is_linked');

?>
<?php
    if (!$isBachelor) {
        ?>
        <li class="home-slider-bachelor-slide">
            <div class="home-slider-bachelor-wrapper">
                <div class="home-slider-bachelor-slide-thumb">
                    <img src="<?php echo $thumbnail['sizes']['home-article']?>" width="<?php echo $thumbnail['sizes']['home-article-width']?>" height="<?php echo $thumbnail['sizes']['home-article-height']?>" alt="<?php echo $name ?>">
                </div>
                <div class="home-slider-bachelor-slide-content">
                    <h4 class="fourth-title">
                        <?php echo $name ?>
                    </h4>
                    <a href="<?php echo the_permalink($post->ID) ?>" class="button white">
                        <?php _e('Découvrir') ?>
                    </a>
                </div>
            </div>
        </li>
        <?php
    }
?>