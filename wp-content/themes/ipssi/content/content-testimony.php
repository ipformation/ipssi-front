<?php
/**
 * The template for Testimony
 */


$fields = get_post_custom(($testimony->ID));
$image = wp_get_attachment_image($fields['img'][0]);
$author = $fields['author'][0];
$role = $fields['role'][0];
$content = $fields['content'][0];

?>

<li class="bachelor-testimony-list-item">
    <div class="bachelor-testimony-header">
        <div class="bachelor-testimony-picture">
            <?php
             echo $image
            ?>
        </div>
        <div class="bachelor-testimony-author">
            <span class="bachelor-testimony-author-name">
                <?php echo $author; ?>
            </span>
            <span class="bachelor-testimony-author-role">
                <?php echo $role; ?>
            </span>
        </div>
    </div>
    <div class="bachelor-testimony-content">
        <?php echo $content; ?>
    </div>
</li>