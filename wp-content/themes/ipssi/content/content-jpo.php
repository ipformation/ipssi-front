<?php
/**
 * The template for displaying CTAs
 */

$campus = get_field('place');
$JPOs = (array)get_field('jpo');

?>

<?php
    if ($JPOs[0]) {
        ?>
        <li class="home-jpo-list-item">
            <a class="home-jpo-item-campus" href="<?php the_permalink($post) ?>">
                <?php echo $campus ?>
            </a>
            <?php
            foreach ($JPOs as $jpo) {
                $jpoField = get_post_custom($jpo->ID);
                $date = $jpoField['date'][0];
                $date = \DateTime::createFromFormat('d/m/Y', $date);
                $date = date_i18n("d F", strtotime($date->format('Y-m-d')));
                $hours = $jpoField['hours'][0];
                ?>
                <p class="home-jpo-item-date">
                    <?php echo  $date ?>
                    <span>
            <?php echo  $hours ?>
        </span>
                </p>
                <?php
            }
            ?>

        </li>
        <?php
    }
?>
