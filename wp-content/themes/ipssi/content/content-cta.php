<?php
/**
 * The template for displaying CTAs
 */
$link = get_field('link');
$wording = get_field('button');
$icon = get_field('icon');

?>

<li class="home-slider-cta-list-item cta-list-item">
    <a class="button orange <?php echo $icon ?>" href="<?php echo  $link ?>" title="<?php echo  $wording ?>">
        <img class="svg" src="<?php echo get_template_directory_uri()."/assets/images/icon-$icon.svg"?>" width="49" height="45" alt="<?php _e('Profil Concerné') ?>">
        <?php echo  $wording ?>
    </a>
</li>