<?php
/**
 * The template for Contact form
 */
?>

<section class="main-contact <?php echo (isset($hasMap)) ? 'has-map' : '' ?>">
    <?php
        if (isset($hasMap)) {
            ?>
            <div class="main-contact-gmap"
                 data-icon="<?php echo get_template_directory_uri().'/assets/images/icon-map.svg'?>"
                 data-content="<?php echo get_field('map_description') ?>"
                 data-lat="<?php echo $map['lat']?>"
                 data-lng="<?php echo $map['lng']?>" id="isefacMap">
            </div>
            <div class="grid">
                <div class="col-1-2">
                    <?php echo  do_shortcode('[contact-form-7 id="92" title="Formulaire de contact"]'); ?>
                </div>
            </div>

            <?php
        }
        else {
            ?>
            <div class="grid">
                <?php echo  do_shortcode('[contact-form-7 id="92" title="Formulaire de contact"]'); ?>
            </div>
            <?php
        }
    ?>
</section>