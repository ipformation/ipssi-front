<?php
/**
 * The template for Testimony in Campus
 */


$fields = get_post_custom(($testimony->ID));
$image = wp_get_attachment_image($fields['img'][0]);
$author = $fields['author'][0];
$role = $fields['role'][0];
$content = $fields['content'][0];

?>

<li class="campus-testimony-list-item">
    <div class="campus-testimony-header">
        <div class="campus-testimony-picture">
            <?php
            echo $image
            ?>
        </div>
        <div class="campus-testimony-author">
            <span class="campus-testimony-author-name">
                <?php echo $author; ?>
            </span>
            <span class="campus-testimony-author-role">
                <?php echo $role; ?>
            </span>
        </div>
    </div>
    <div class="campus-testimony-content">
        <?php echo $content; ?>
    </div>
</li>