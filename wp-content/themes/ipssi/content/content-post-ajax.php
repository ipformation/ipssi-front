<?php
/**
 * The template for displaying Posts From ajaxCall
 */

$id = $post->ID;
$fields = get_post_custom($id);
$category = get_the_category($post->ID);
$postCampus = $fields['campus'][0];
$tags = get_the_tags($id);
$date = $post->post_date;
$date = \DateTime::createFromFormat('Y-m-d H:i:s', $date);
$date = date_i18n("d F Y", strtotime($date->format('Y-m-d')));
?>

<div class="blog-content-post-item all col-1-2 <?php echo (($i%2) == 0) ? 'first' : 'last' ?> cat-<?php echo $category[0]->cat_ID ?> <?php echo ($postCampus !== false)? "campus-$postCampus->ID" : ""?>"
            data-offset="<?php echo $ii ?>">
    <div class="blog-content-post-item-wrapper">
        <a class="blog-content-post-link" href="<?php the_permalink($id) ?>" title="<?php echo $post->post_title ?>">
            <?php echo get_the_post_thumbnail($id, 'blog-article') ?>
            <div class="blog-content-post-item-content">
                <h2 class="second-title">
                    <?php echo $post->post_title ?>
                </h2>
                <p class="blog-content-post-item-content-date">
                    <?php _e('Le', 'webqam') ?> <?php echo $date ?>
                </p>
                <div class="blog-content-post-item-content-tags">
                    <ul>
                        <?php
                        foreach ($tags as $tag) {
                            ?>
                            <li>
                                <?php echo $tag->name; ?>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </a>
    </div>
</div>