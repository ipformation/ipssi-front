//  Include dependencies
var gulp = require('gulp'),
  autoprefixer = require('gulp-autoprefixer'),
  concat = require('gulp-concat'),
  jshint = require('gulp-jshint'),
  cleanCss = require('gulp-clean-css'),
  rename = require('gulp-rename'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  plumber = require('gulp-plumber');

/**
 * Paths for sources to be used with Gulp
 */
var paths = {
  css: {
    src: 'assets/sass/front.scss',
    dest: 'assets/css/',
    watch: ['assets/sass/**/*.scss']
  },
  vendors: {
    src: [
      'assets/js/source/vendors/*.js'
    ],
    dest: 'assets/js/build'
  },
  js: {
    src: [
      'assets/js/source/script.js'
    ],
    dest: 'assets/js/build'
  },
  img: {
    src: ['assets/images/*.png', 'assets/images/*.jpg', 'assets/images/*.jpeg'],
    dest: 'assets/images/'
  }
};

var autoprefixerOptions = {
  browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
  cascade: false
};

/**
 * Styles tasks
 */
gulp.task(
  'styles', function () {
    return gulp.src(paths.css.src)
      .pipe(sourcemaps.init())
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(cleanCss({compatibility: 'ie8'}))
      .pipe(rename({suffix: '.min'}))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(paths.css.dest));
  });

/**
 * Script task
 */
gulp.task(
  'scripts', function () {
    return gulp.src(paths.js.src)
      .pipe(
        plumber(
          {
            handleError: function (err) {
              console.log(err);
              this.emit('end');
            }
          }))
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'))
      .pipe(sourcemaps.init())
      .pipe(concat('scripts.js'))
      .pipe(rename({suffix: '.min'}))
      .pipe(uglify())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(paths.js.dest));
  });

/**
 * Vendors task
 */
gulp.task(
    'scripts-vendors', function () {
      return gulp.src(paths.vendors.src)
          .pipe(
          plumber(
              {
                handleError: function (err) {
                  console.log(err);
                  this.emit('end');
                }
              }))
          .pipe(sourcemaps.init())
          .pipe(concat('vendors.js'))
          .pipe(rename({suffix: '.min'}))
          .pipe(uglify())
          .pipe(sourcemaps.write('.'))
          .pipe(gulp.dest(paths.vendors.dest));
    });


gulp.task(
  'watch', function () {
    gulp.watch(paths.css.watch.concat(paths.css.src), ['styles']);
    gulp.watch(paths.vendors.src, ['scripts-vendors']);
    gulp.watch(paths.js.src, ['scripts']);
  });

/**
 * Image task
 */

gulp.task(
    'img', function () {
      return gulp.src(paths.img.src)
          .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
          }))
          .pipe(gulp.dest(paths.img.dest));
    });

gulp.task(
  'default', [
    'scripts-vendors',
    'scripts',
    'styles'
  ]);
