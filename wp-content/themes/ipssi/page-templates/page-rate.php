<?php
/*
Template Name: Page-Tarif
*/

get_header();

$title = get_field('title');
$fundingTitle = get_field('first_section_title');
$fundingSubTitle = get_field('first_section_subtitle');
$feeTitle = get_field('second_section_title');
$feeContent = get_field('second_section_content');
$feeCondition = get_field('rate_conditions');

?>
    <div class="main-content rate">
        <section class="rate-banner">
            <?php echo get_the_post_thumbnail(); ?>
            <div class="rate-banner-content-wrapper">
                <div class="rate-banner-content grid">
                    <h1 class="main-title">
                        <?php echo $title ?>
                    </h1>
                    <ul class="rate-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="rate-funding">
            <div class="grid">
                <h2 class="second-title">
                    <?php echo  $fundingTitle ?>
                </h2>
                <p>
                    <?php echo  $fundingSubTitle ?>
                </p>
                <ul class="rate-funding-list">
                    <?php query_posts(array('post_type' => 'funding','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                        get_template_part( 'content/content', 'funding' );
                    endwhile; endif; wp_reset_query(); ?>
                </ul>
            </div>
        </section>
        <section class="rate-fee">
            <div class="grid">
                <h2 class="second-title">
                    <?php echo $feeTitle ?>
                </h2>
                <div class="rate-fee-content">
                    <?php echo $feeContent ?>
                    <p class="rate-fee-content-condition">
                        <?php echo $feeCondition ?>
                    </p>
                </div>
            </div>
        </section>
        <?php
        get_template_part( 'content/content', 'contact');
        ?>
    </div>
<?php
get_footer();
