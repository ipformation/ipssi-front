<?php
/*
Template Name: Page-Metier
*/

get_header();

?>
    <div class="main-content work">
        <section class="work-banner">
            <?php echo get_the_post_thumbnail(); ?>
            <div class="work-banner-content-wrapper">
                <div class="work-banner-content grid">
                    <h1 class="main-title">
                        <?php echo the_title() ?>
                    </h1>
                    <ul class="work-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="work-content">
            <div class="grid">
                <?php the_content() ?>
            </div>
        </section>
        <section class="work-taxonomy">
            <div class="grid">
                <ul class="work-taxonomy-list">

                    <?php
                    $i = 0;
                    $args = array(
                        'taxonomy' => 'work_type',
                        'orderby' => 'menu_order'
                    );
                    $taxonomies = get_categories($args);
                    foreach ($taxonomies as $taxonomy) {
                        include(locate_template('content/content-work-taxonomy.php'));
                        $i++;
                    }
                    ?>
                </ul>
            </div>
        </section>
        
    </div>
<?php
get_footer();
