<?php
/*
Template Name: Page-School
*/

get_header();

$title = get_field('title');
$testimony = (array)get_field('testimony');
$testimonyContent = get_post_custom($testimony['ID']);
$steps = get_field('steps');

?>
    <div class="main-content school">
        <section class="school-banner">
            <?php echo get_the_post_thumbnail(); ?>
            <div class="school-banner-content-wrapper">
                <div class="school-banner-content grid">
                    <h1 class="main-title">
                        <?php echo $title ?>
                    </h1>
                    <ul class="school-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="school-testimony">
            <div class="grid">
                <div class="school-testimony-wrapper">
                    <div class="school-testimony-header">
                        <div class="school-testimony-picture">
                            <?php echo wp_get_attachment_image($testimonyContent['img'][0]); ?>
                        </div>
                        <div class="school-testimony-author">
                            <h2 class="second-title">
                                <?php echo $testimonyContent['author'][0]; ?>
                            </h2>
                        </div>
                        <div class="school-testimony-role">
                            <?php echo $testimonyContent['role'][0]; ?>
                        </div>
                    </div>
                    <div class="school-testimony-content">
                        <?php echo $testimonyContent['content'][0]; ?>
                    </div>
                </div>
            </div>
        </section>

        <?php
         if ($steps) {
             ?>
             <section class="school-steps">
                 <div class="school-steps-list-wrapper">
                     <div class="grid">
                         <ul class="school-steps-list">
                             <?php
                             $i = 1;
                             query_posts(array('post_type' => 'steps','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                                 include(locate_template('content/content-steps.php'));
                                 $i++;
                             endwhile; endif; wp_reset_query(); ?>
                         </ul>
                     </div>
                 </div>
             </section>
             <?php
         }
        ?>

    </div>
<?php
get_footer();
