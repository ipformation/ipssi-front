<?php
/*
Template Name: Page-Documentation
*/

get_header();
$subtitle = get_field('subtitle');
$success = get_field('success');
$documentationFile = get_field('documentation');
$dossierFile = get_field('dossier');

?>
<div class="main-content documentation">
    <section class="documentation-banner">
        <?php echo get_the_post_thumbnail(); ?>
        <div class="documentation-banner-content-wrapper">
            <div class="documentation-banner-content grid">
                <h1 class="main-title">
                    <?php echo the_title() ?>
                </h1>
            </div>
        </div>
    </section>
    <section class="documentation-content">
        <div class="grid documentation-content-wrapper">
            <h2 class="second-title">
                <?php echo $subtitle ?>
            </h2>
            <?php echo do_shortcode($post->post_content); ?>
        </div>
        <div class="documentation-success">
            <div class="grid">
                <div class="documentation-success-wrapper">
                    <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-check.svg'?>" width="64" height="57" alt="Success">
                    <div class="documentation-success-courriel">
                        <?php echo $success ?>
                    </div>
                    <div class="documentation-success-download">
                        <div class="documentation-success-download-documentation">
                            <h3 class="third-title">
                                <?php _e('Documentation','webqam') ?>
                            </h3>
                            <a href="<?php echo $documentationFile['url']; ?>" class="button white download" download target="_blank">
                                <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-twitter.svg'?>" width="16" height="16" alt="Twitter">
                                <?php _e('Télécharger','webqam') ?>
                            </a>
                        </div>
                        <div class="documentation-success-download-dossier">
                            <h3 class="third-title">
                                <?php _e('Dossier de candidature','webqam') ?>
                            </h3>
                            <a href="<?php echo $dossierFile['url']; ?>" class="button white download" download target="_blank">
                                <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-twitter.svg'?>" width="16" height="16" alt="Twitter">
                                <?php _e('Télécharger','webqam') ?>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
get_footer();
