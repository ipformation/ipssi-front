<?php
/*
Template Name: Page-JPO
*/

get_header();
$subtitle = get_field('subtitle');
$success = get_field('success');
?>
<div class="main-content jpo">
    <section class="jpo-banner">
        <?php echo get_the_post_thumbnail(); ?>
        <div class="jpo-banner-content-wrapper">
            <div class="jpo-banner-content grid">
                <h1 class="main-title">
                    <?php echo the_title() ?>
                </h1>
            </div>
        </div>
    </section>
    <section class="jpo-content">
        <div class="grid jpo-content-wrapper">
            <h2 class="second-title">
                <?php echo $subtitle ?>
            </h2>
            <ul class="jpo-content-list">
                <?php
                $i = 1;
                query_posts(array('post_type' => 'campus','orderby' => 'menu')); if(have_posts()) : while(have_posts()) : the_post();
                    include(locate_template('content/content-form-jpo.php'));
                    $i++;
                endwhile; endif; wp_reset_query(); ?>
            </ul>
            <?php echo do_shortcode($post->post_content); ?>
        </div>
        <div class="jpo-success">
            <div class="grid">
                <div class="jpo-success-wrapper">
                    <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-check.svg'?>" width="64" height="57" alt="Success">
                    <?php echo $success ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
get_footer();
