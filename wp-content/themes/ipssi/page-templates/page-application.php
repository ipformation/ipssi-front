<?php
/*
Template Name: Offres d'emploi
*/
get_header(); ?>

<div id="main-content" class="main-content">
    <div class="main-content application">
        <section class="application-banner">
            <?php echo get_the_post_thumbnail($pageId) ?>
            <div class="application-banner-content-wrapper">
                <div class="application-banner-content grid">
                    <h1 class="main-title">
                        <?php echo get_the_title($pageId) ?>
                    </h1>
                    <ul class="application-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="application-content">
            <div class="application-content-post-list">
                <div class="grid">
                    <div class="application-content-post-item">
                        <div class="application-content-post-item-wrapper">
                        <?php query_posts(array('post_type' => 'application','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'application' );
                        endwhile; endif; wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div><!-- .content-area -->
</div>
<?php get_footer(); ?>