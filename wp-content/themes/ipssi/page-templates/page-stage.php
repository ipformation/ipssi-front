<?php
/*
Template Name: Page-Stage
*/

get_header();
$firstCta = get_field('first_cta');
$firstLink = get_field('first_link');
$firstIcon = get_field('first_icon');


$secondCta = get_field('second_cta');
$secondLink = get_field('second_link');
$secondIcon = get_field('second_icon');

$content = get_field('content');
?>
    <div class="main-content stage">
        <section class="stage-banner">
            <?php echo get_the_post_thumbnail(); ?>
            <div class="stage-banner-content-wrapper">
                <div class="stage-banner-content grid">
                    <h1 class="main-title">
                        <?php echo the_title() ?>
                    </h1>
                    <ul class="stage-banner-content-cta-list">
                        <li class="stage-banner-content-cta-list-item cta-list-item stage">
                            <a class="button orange <?php echo $firstIcon ?>" href="<?php echo  $firstLink ?>" title="<?php echo  $firstCta ?>">
                                <img class="svg" src="<?php echo get_template_directory_uri()."/assets/images/icon-$firstIcon.svg"?>" width="49" height="45" alt="<?php echo $firstCta ?>">
                                <?php echo $firstCta ?>
                            </a>
                        </li>
                        <li class="stage-banner-content-cta-list-item cta-list-item stage consult">
                            <a class="button orange <?php echo $secondIcon ?>" href="<?php echo  $secondLink ?>" title="<?php echo  $secondCta ?>">
                                <img class="svg" src="<?php echo get_template_directory_uri()."/assets/images/icon-$secondIcon.svg"?>" width="49" height="45" alt="<?php echo $secondCta ?>">
                                <?php echo $secondCta ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="stage-content">
            <div class="grid">
                <?php echo $content ?>
            </div>
        </section>
        <?php
        get_template_part( 'content/content', 'contact');
        ?>
    </div>
<?php
get_footer();
