<?php
/*
Template Name: Page-Cursus
*/

get_header();

$title = get_field('title');
$firstSectionTitle = get_field('first_section_title');
$firstSectionSubTitle = get_field('first_section_subtitle');
$firstSectionCursus = (array)get_field('first_section_cursus');

$secondSectionTitle = get_field('second_section_title');
$secondSectionSubTitle = get_field('second_section_subtitle');
$secondSectionCursus = (array)get_field('second_section_cursus');

$thirdSectionTitle = get_field('third_section_title');
$thirdSectionSubTitle = get_field('third_section_subtitle');
$thirdSectionCursus = (array)get_field('third_section_cursus');

?>
    <div class="main-content cursus">
        <section class="cursus-banner">
            <?php echo get_the_post_thumbnail(); ?>
            <div class="cursus-banner-content-wrapper">
                <div class="cursus-banner-content grid">
                    <h1 class="main-title">
                        <?php echo $title ?>
                    </h1>
                    <ul class="cursus-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="cursus-first-section cursus-section">
            <div class="grid">
                <h2 class="second-title">
                    <?php echo $firstSectionTitle ?>
                </h2>
                <?php echo $firstSectionSubTitle?>
                <ul class="cursus-section-list">
                    <?php
                    foreach ($firstSectionCursus as $cursus) {
                        include(locate_template('content/content-cursus.php'));
                    }
                    ?>
                </ul>

            </div>
        </section>
        <section class="cursus-second-section cursus-section">
            <div class="grid">
                <h2 class="second-title">
                    <?php echo $secondSectionTitle ?>
                </h2>
                <?php echo $secondSectionSubTitle ?>
                <ul class="cursus-section-list">
                    <?php
                    foreach ($secondSectionCursus as $cursus) {
                        include(locate_template('content/content-cursus.php'));
                    }
                    ?>
                </ul>

            </div>
        </section>
        <section class="cursus-third-section cursus-section">
            <div class="grid">
                <h2 class="second-title">
                    <?php echo $thirdSectionTitle ?>
                </h2>
                <?php echo $thirdSectionSubTitle?>
                <ul class="cursus-section-list">
                    <?php
                    foreach ($thirdSectionCursus as $cursus) {
                        include(locate_template('content/content-cursus.php'));
                    }
                    ?>
                </ul>

            </div>
        </section>
    </div>
<?php
get_footer();
