<?php
/*
Template Name: Page-Home
*/

get_header();

$sliderTitle = strip_tags(get_field('slider_title'), '<strong><b>');
$sliderDescription = get_field('slider_description');
$sliderGal = (array)get_field('slider_images');

$bachelorTitle = strip_tags(get_field('bachelor_title'), '<strong><b>');
$zoomTitle = strip_tags(get_field('titre_zoom'), '<strong><b>');
$workshopTitle = strip_tags(get_field('workshop_title'), '<strong><b>');

$campusTitle = strip_tags(get_field('campus_title'), '<strong><b>');
$campusImage = get_field('campus_image');
$campusDescription = get_field('campus_description');
$campusBtn = get_field('campus_button');

$eventTitle = strip_tags(get_field('event_title'), '<strong><b>');
$eventBtn = get_field('event_button');
$eventLink = get_field('event_link');

$JPOTitle = strip_tags(get_field('jpo_title'), '<strong><b>');
$JPOSubTitle = get_field('jpo_subtitle');
$JPOImage = get_field('jpo_image');
$JPOBtn = get_field('jpo_button');
$JPOLink = get_field('jpo_link');

?>
    <div id="page-home" class="main-content">

        <section class="home-main-slider">
            <ul class="home-slider">
                <?php
                foreach ($sliderGal as $slide) {
                    ?>
                    <li class="home-slider-image">
                        <img src="<?php echo $slide['url'] ?>" alt="<?php echo $sliderTitle ?>" width="<?php echo $slide['width'] ?>" height="<?php echo $slide['height'] ?>">
                        <div class="home-slider-content">
                            <h1 class="main-title">
                                <?php echo $sliderTitle ?>
                            </h1>
                            <h2 class="second-title">
                                <ul class="home-slider-bachelor-list">
                                    <?php
                                    $args = array(
                                        'taxonomy' => 'cpt_type',
                                        'orderby' => 'menu_order'
                                    );
                                    $taxonomies = get_categories($args);
                                    $i = 0;
                                    foreach ($taxonomies as $taxonomy) {
                                        include(locate_template('content/content-bachelor-list.php'));
                                        $i++;
                                    };
                                    ?>
                                </ul>
                            </h2>
                            <div class="home-slider-cta">
                                <ul class="home-slider-cta-list">
                                    <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                                        get_template_part( 'content/content', 'cta' );
                                    endwhile; endif; wp_reset_query(); ?>
                                </ul>
                            </div>
                        </div>
                        <div class="home-slider-campus">
                            <ul class="home-slider-campus-list">
                                <?php
                                $i = 0;
                                query_posts(array('post_type' => 'campus','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                                if ($i !== 0 && $i%4 == 0) {
                                ?>
                                <span class="campus-list-sep"> - </span>
                            </ul>
                            <ul class="home-slider-campus-list">
                                <?php
                                }
                                elseif ($i !== 0) {
                                    ?>
                                    -
                                    <?php
                                }
                                get_template_part( 'content/content', 'campus' );
                                $i++;
                                endwhile; endif; wp_reset_query(); ?>
                            </ul>
                        </div>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </section>

        <section class="home-slider-bachelor">
            <div class="grid">
                <h3 class="third-title">
                    <?php echo $bachelorTitle ?>
                </h3>
                <ul class="home-bachelor-slider">
                    <?php query_posts(array('post_type' => 'bachelor','orderby' => 'menu')); if(have_posts()) : while(have_posts()) : the_post();
                        get_template_part( 'content/content', 'bachelor' );
                    endwhile; endif; wp_reset_query(); ?>
                </ul>
            </div>
        </section>

        <?php query_posts(array('post_type' => 'post','category_name' => 'zoom', 'posts_per_page' => 1)); if(have_posts()) : while(have_posts()) : the_post();
            include(locate_template('content/content-zoom.php'));
        endwhile; endif; wp_reset_query(); ?>

        <section class="home-workshop">
            <div class="grid">
                <h3 class="third-title">
                    <?php echo $workshopTitle ?>
                </h3>
                <div class="home-workshop-content">
                    <?php
                    $i = 0;
                    query_posts(array('post_type' => 'workshop','orderby' => 'menu')); if(have_posts()) : while(have_posts()) : the_post();
                        $name = get_field('name');
                        ?>
                        <span class="home-workshop-item item-<?php echo  $i ?>">
                            <?php echo  $name ?>
                        </span>
                        <?php
                        $i++;
                    endwhile; endif; wp_reset_query(); ?>
                </div>
            </div>
        </section>

        <section class="home-campus" style="background-image: url('<?php echo  $campusImage['url'] ?>')">
            <div class="home-campus-wrapper">
                <div class="grid">
                    <div class="col-1-2">
                        <h3 class="third-title">
                            <?php echo $campusTitle ?>
                        </h3>
                        <div class="home-campus-thumbnail">
                            <img src="<?php echo $campusImage['url'] ?>" alt="<?php echo  $campusTitle ?>" />
                        </div>
                        <div class="home-campus-description">
                            <?php echo $campusDescription ?>
                        </div>
                        <div class="home-campus-cta">
                            <a class="button white" href="">
                                <?php echo $campusBtn ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="home-events">
            <div class="grid">
                <h3 class="third-title">
                    <?php echo $eventTitle ?>
                </h3>
                <ul class="home-events-slider">
                    <?php query_posts(array('post_type' => 'post','orderby' => 'menu', 'category_name' => 'events', 'posts_per_page' => 3)); if(have_posts()) : while(have_posts()) : the_post();
                        get_template_part( 'content/content', 'events' );
                    endwhile; endif; wp_reset_query(); ?>
                </ul>
                <a href="<?php echo $eventLink ?>" class="button full-white">
                    <?php echo $eventBtn ?>
                </a>
            </div>
        </section>

        <section class="home-jpo" style="background: #fff url('<?php echo  $JPOImage['url'] ?>') no-repeat 100% 100%">
            <div class="home-jpo-content">
                <div class="grid">
                    <div class="col-1-2">
                        <h3 class="third-title">
                            <?php echo $JPOTitle ?>
                        </h3>
                        <h4 class="fourth-title">
                            <?php echo $JPOSubTitle ?>
                        </h4>
                        <ul class="home-jpo-list">
                            <?php query_posts(array('post_type' => 'campus','orderby' => 'menu')); if(have_posts()) : while(have_posts()) : the_post();
                                get_template_part( 'content/content', 'jpo' );
                            endwhile; endif; wp_reset_query(); ?>
                        </ul>
                        <a class="button white" href="<?php echo  $JPOLink ?>" title="<?php echo $JPOBtn ?>">
                            <?php echo $JPOBtn ?>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php
        get_template_part( 'content/content', 'contact');
        ?>
    </div>
    <aside class="aside-cta hidden">
        <ul class="aside-cta-list">
            <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                get_template_part( 'content/content', 'cta' );
            endwhile; endif; wp_reset_query(); ?>
        </ul>
    </aside>
<?php
get_footer();
