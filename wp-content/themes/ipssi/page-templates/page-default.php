<?php
/*
Template Name: Page-Default
*/

get_header();

?>
<div class="main-content default">
    <section class="default-banner">
        <?php echo get_the_post_thumbnail(); ?>
        <div class="default-banner-content-wrapper">
            <div class="default-banner-content grid">
                <h1 class="main-title">
                    <?php echo the_title() ?>
                </h1>
            </div>
        </div>
    </section>
    <section class="default-content">
        <div class="grid">
            <?php echo $post->post_content; ?>
        </div>
    </section>
</div>
<?php
get_footer();
