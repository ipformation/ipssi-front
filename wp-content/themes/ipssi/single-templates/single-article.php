<?php

get_header();

// Custom form type description
$description = get_field('description');
// Image from ERP
$image = get_field('img_url');
?>
    <div class="main-content post">
        <section class="post-banner">
            <img src="<?php echo $image; ?>">
        </section>
        <section class="post-content">
            <div class="grid">
                <h1 class="main-title">
                    <?php echo the_title()?>
                </h1>
                <div class="post-content-content">
                    <?php echo $description ?>
                </div>

            </div>
        </section>
    </div>
<?php
get_footer();
