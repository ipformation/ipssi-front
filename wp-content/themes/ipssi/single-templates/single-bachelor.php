<?php

get_header();
$title = get_field('name');

$infoRequirement = get_field('info_requirement');
$infoDuration = get_field('info_duration');
$infoType = get_field('info_type');
$description = get_field('description');
$program = get_field('program');
$admission = get_field('admission');
$profession = get_field('profession');
$after = get_field('after');
$testimonies = (array)get_field('testimony');

?>
    <div class="main-content bachelor">
        <section class="bachelor-banner">
            <?php echo get_the_post_thumbnail(); ?>
            <div class="bachelor-banner-content-wrapper">
                <div class="bachelor-banner-content grid">
                    <h1 class="main-title">
                        <?php echo $title ?>
                    </h1>
                    <ul class="bachelor-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>

        <section class="bachelor-content">
            <div class="grid">
                <aside class="bachelor-content-aside col-1-3">
                    <ul class="bachelor-content-aside-list">
                        <?php query_posts(array('post_type' => 'bachelor','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            include(locate_template('content/content-aside-bachelor.php'));
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </aside>
                <div class="bachelor-content-wrapper col-2-3">
                    <div class="bachelor-content-jpo">
                        <h3 class="third-title">
                            <?php _e('Consulter les journées porte ouverte pour ce bachelor') ?>
                        </h3>
                        <a href="" class="button full-white">
                            <?php _e('Consulter') ?>
                        </a>
                    </div>
                    <div class="bachelor-content-info">
                        <ul class="bachelor-content-info-list">
                            <li class="bachelor-content-info-list-item bachelor-content-info-requirement">
                                <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-bachelor-profil.svg'?>" width="49" height="45" alt="<?php _e('Profil Concerné') ?>">
                                <h4 class="fourth-title">
                                    <?php _e('Profil Concerné') ?>
                                </h4>
                                <p class="bachelor-content-description">
                                    <?php echo $infoRequirement ?>
                                </p>
                            </li>
                            <li class="bachelor-content-info-list-item bachelor-content-info-duration">
                                <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-bachelor-calendar.svg'?>" width="49" height="45" alt="<?php _e('Durée du cursus') ?>">
                                <h4 class="fourth-title">
                                    <?php _e('Durée du cursus') ?>
                                </h4>
                                <p class="bachelor-content-description">
                                    <?php echo $infoDuration ?>
                                </p>
                            </li>
                            <li class="bachelor-content-info-list-item bachelor-content-info-type">
                                <img class="svg" src="<?php echo get_template_directory_uri().'/assets/images/icon-bachelor-diplome.svg'?>" width="49" height="45" alt="<?php _e('Type de diplôme') ?>">
                                <h4 class="fourth-title">
                                    <?php _e('Type de diplôme') ?>
                                </h4>
                                <p class="bachelor-content-description">
                                    <?php echo $infoType ?>
                                </p>
                            </li>
                        </ul>
                    </div>

                    <div class="bachelor-content-inner bachelor-content-inner-description">
                        <div class="bachelor-content-header bachelor-content-description-header">
                            <h3 class="third-title">
                                <?php _e('En Bref') ?>
                            </h3>
                        </div>
                        <div class="bachelor-content-content bachelor-content-description-content">
                            <?php echo $description; ?>
                        </div>
                    </div>

                    <div class="bachelor-content-inner bachelor-content-inner-program">
                        <div class="bachelor-content-header bachelor-content-program-header">
                            <h3 class="third-title">
                                <?php _e('Programme') ?>
                            </h3>
                        </div>
                        <div class="bachelor-content-content bachelor-content-program-content">
                            <?php echo $program; ?>
                        </div>
                    </div>

                    <div class="bachelor-content-inner bachelor-content-inner-admission">
                        <div class="bachelor-content-header bachelor-content-admission-header">
                            <h3 class="third-title">
                                <?php _e('Admissions') ?>
                            </h3>
                        </div>
                        <div class="bachelor-content-content bachelor-content-admission-content">
                            <?php echo $admission; ?>
                        </div>
                    </div>

                    <div class="bachelor-content-inner bachelor-content-inner-profession">
                        <div class="bachelor-content-header bachelor-content-profession-header">
                            <h3 class="third-title">
                                <?php _e('Métiers') ?>
                            </h3>
                        </div>
                        <div class="bachelor-content-content bachelor-content-profession-content">
                            <?php echo $profession; ?>
                        </div>
                    </div>

                    <div class="bachelor-content-inner bachelor-content-inner-after">
                        <div class="bachelor-content-header bachelor-content-after-header">
                            <h3 class="third-title">
                                <?php _e('Et après ?') ?>
                            </h3>
                        </div>
                        <div class="bachelor-content-content bachelor-content-after-content">
                            <?php echo $after; ?>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="bachelor-testimony">
            <div class="grid">
                <ul class="bachelor-testimony-list">
                    <?php
                    foreach ($testimonies as $testimony) {
                        include(locate_template('content/content-testimony.php'));
                    }
                    ?>
                </ul>
            </div>
        </section>

        <?php
        get_template_part( 'content/content', 'contact');
        ?>

    </div>
<?php
get_footer();
