<?php

get_header();

$title = get_field('place');
$JPOs = (array)get_field('jpo');
$description = get_field('description');
$bachelorTitle = get_field('bachelor_title');
$bachelors = (array)get_field('bachelors');
$testimonies = (array)get_field('testimonies');
$map = get_field('map');
$hasMap = true;
?>
    <div class="main-content campus">
        <section class="campus-banner">
            <?php echo get_the_post_thumbnail(); ?>
            <div class="campus-banner-content-wrapper">
                <div class="campus-banner-content grid">
                    <h1 class="main-title">
                        <?php echo $title ?>
                    </h1>
                    <ul class="campus-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>

        <section class="campus-content">
            <div class="grid">
                <aside class="campus-content-aside col-1-3">
                    <ul class="campus-content-aside-list">
                        <?php query_posts(array('post_type' => 'campus','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            include(locate_template('content/content-aside-campus.php'));
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </aside>
                <div class="campus-content-wrapper col-2-3">
                    <?php
                        if ($JPOs) {
                            ?>
                            <?php
                                foreach ($JPOs as $jpo) {
                                    $fields = get_post_custom($jpo->ID);
                                    ?>
                                    <div class="campus-content-jpo">
                                        <p class="campus-content-jpo-content">
                                            <b><?php _e('Journée porte ouverte le', 'webqam') ?></b>
                                            <span class="campus-content-jpo-content-day">
                                                <?php
                                                $date = $fields['date'][0];
                                                $date = \DateTime::createFromFormat('d/m/Y', $date);
                                                $date = date_i18n("d F Y", strtotime($date->format('Y-m-d')));
                                                echo $date; ?>
                                            </span>
                                            <span class="campus-content-jpo-content-hour">
                                                <?php echo $fields['hours'][0]; ?>
                                            </span>
                                        </p>
                                        <a href="" class="button full-white">
                                            <?php _e('M\'inscrire', 'webqam') ?>
                                        </a>
                                    </div>
                                    <?php
                                }
                            ?>
                            <?php
                        }
                    ?>

                    <div class="campus-content-info">
                        <?php
                            echo $description;
                        ?>
                    </div>

                    <div class="campus-bachelor">
                        <h2 class="second-title">
                            <?php
                                echo $bachelorTitle;
                            ?>
                        </h2>
                        <ul class="campus-bachelor-list">

                            <?php
                            $i = 0;
                            $args = array(
                                'taxonomy' => 'cpt_type',
                                'orderby' => 'menu_order'
                            );
                            $taxonomies = get_categories($args);
                            foreach ($taxonomies as $taxonomy) {
                                include(locate_template('content/content-campus-bachelor.php'));
                                $i++;
                            }
                            ?>
                        </ul>
                    </div>

                    <div class="campus-testimony">
                        <ul class="campus-testimony-list">
                            <?php
                            foreach ($testimonies as $testimony) {
                                include(locate_template('content/content-campus-testimony.php'));
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <?php
        include(locate_template('content/content-contact.php'));
        ?>

    </div>
<?php
get_footer();
