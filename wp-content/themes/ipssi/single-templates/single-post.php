<?php

get_header();

$campus = get_field('campus');
$banner = get_field('banner');
$subtitle = get_field('subtitle');
$content = get_field('content');
$category = get_the_category($post->ID);
?>
    <div class="main-content post">
        <section class="post-banner">
            <img src="<?php echo $banner['url'] ?>" width="<?php echo $banner['width'] ?>" height="<?php echo $banner['height'] ?>" alt="">
        </section>
        <section class="post-content">
            <div class="grid">
                <h1 class="main-title">
                    <?php echo the_title()?>
                </h1>
                <h2 class="second-title">
                    <?php echo $subtitle ?>
                </h2>

                <div class="post-content-content">
                    <?php echo $content ?>
                </div>

            </div>
        </section>
        <section class="post-related">
            <div class="grid">
                <h3 class="third-title">
                    <?php _e('En savoir plus', 'ipssi') ?>
                </h3>
                <ul class="post-related-list">
                    <?php
                    query_posts(array('post_type' => 'post','orderby' => 'menu', 'cat' => $category[0]->cat_ID, 'posts_per_page' => 3)); if(have_posts()) : while(have_posts()) : the_post();
                        get_template_part( 'content/content', 'related' );
                    endwhile; endif; wp_reset_query(); ?>
                </ul>
            </div>
        </section>
    </div>
<?php
get_footer();
