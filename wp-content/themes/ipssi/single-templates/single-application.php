<?php
$content = get_field('content');
$title = get_field('title');
$erp_offer_id = get_field('offer_id');
$idOffreEnCours = $post->ID;

get_header(); ?>

    <div class="main-content application">
        <section class="application-banner">
            <?php echo get_the_post_thumbnail($pageId) ?>
            <div class="application-banner-content-wrapper">
                <div class="application-banner-content grid">
                    <h1 class="main-title">
                        <?php echo get_the_title($pageId) ?>
                    </h1>
                    <ul class="application-banner-content-cta-list">
                        <?php query_posts(array('post_type' => 'cta','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                            get_template_part( 'content/content', 'cta' );
                        endwhile; endif; wp_reset_query(); ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="application-section">
            <div class="grid">
                <h1 class="application-description">
                    <?php
                    echo $content
                    ?>
                </h1>
                <div role="form" class="wpcf8" id="applicationForm" lang="fr-FR" dir="ltr">
                    <form action="/application-form.php" method="post" class="wpcf8-form" enctype="multipart/form-data">
                        <div class="form-control">
                            <label for="nom">
                                <span class="content">Nom*</span>
                            </label>
                        <span class="wpcf8-form-control-wrap your-name">
                            <input required="required" type="text" name="nom" value="" size="40" class="wpcf8-form-control wpcf8-text wpcf8-validates-as-required" id="nom" aria-required="true" aria-invalid="false">
                        </span>
                        </div>
                        <div class="form-control">
                            <label for="prenom">
                                <span class="content">Prénom*</span>
                            </label>
                        <span class="wpcf8-form-control-wrap your-name">
                            <input required="required" type="text" name="prenom" value="" size="40" class="wpcf8-form-control wpcf8-text wpcf8-validates-as-required" id="prenom" aria-required="true" aria-invalid="false">
                        </span>
                        </div>
                        <div class="form-control">
                            <label for="email">
                                <span class="content">Email*</span>
                            </label>
                            <span class="wpcf8-form-control-wrap your-email">
                                <input required="required" type="email" name="email" value="" size="40" class="wpcf8-form-control wpcf8-text wpcf8-email wpcf8-validates-as-required wpcf8-validates-as-email" id="email" aria-required="true" aria-invalid="false">
                            </span>
                        </div>
                        <div class="form-control">
                            <label for="metier">
                                <span class="content">Métier</span>
                            </label>
                            <span class="wpcf8-form-control-wrap">
                                <select required class="application-work-select" name="metier" id="metier">
                                    <option value=""></option>
                                    <option value="développeur">Développeur</option>
                                    <option value="intégrateurr">Intégrateur</option>
                                    <option value="graphiste">Graphiste</option>
                                </select>
                            </span>
                        </div>
                        <div class="form-control">
                            <label for="cv">
                                <span class="content">CV*</span>
                            </label>
                            <input required="required" type="file" class="wpcf8-form-control wpcf8-form-control-file" name="cv">
                            <input type="hidden" name="offreId" value="<?php echo $idOffreEnCours; ?>">
                            <input type="hidden" name="erp_offer_id" value="<?php echo $erp_offer_id; ?>">
                        </div>
                        <div class="form-submit">
                            <input type="submit" value="Envoyer" class="wpcf8-form-control wpcf8-submit button orange">
                            <img class="ajax-loader" src="http://ipssi-front.com/wp-content/themes/ipssi/assets/images/loader.png" alt="Sending ..." style="visibility: hidden;">
                        </div>
                        <div class="wpcf8-response-output wpcf8-display-none"></div>
                    </form>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>