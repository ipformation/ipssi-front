<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

$theme_locations = get_nav_menu_locations();
$menuSubscribe = get_term( $theme_locations["footer-nav-subscribe"], 'nav_menu' );
$menuSchool = get_term( $theme_locations["footer-nav-school"], 'nav_menu' );
$menuContact = get_term( $theme_locations["footer-nav-contact"], 'nav_menu' );
?>
<footer class="site-footer" role="contentinfo">
    <div class="grid">
        <div class="site-footer-identity col-3-12">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site-logo">
                <img src="<?php echo get_theme_mod("logo")?>" alt="<?php bloginfo("name") ?>" />
            </a>
        </div>
        <div class="site-footer-nav col-9-12">
            <ul class="site-footer-list">
                <li class="site-footer-list-item site-footer-list-item-left">
                    <h4 class="title-fourth">
                        <?php echo  $menuSubscribe->name ?>
                    </h4>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-subscribe', 'menu_class' => 'footer-nav-menu' ) ); ?>
                </li>
                <li class="site-footer-list-item site-footer-list-item-middle">
                    <h4 class="title-fourth">
                        <?php echo  $menuSchool->name ?>
                    </h4>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-school', 'menu_class' => 'footer-nav-menu' ) ); ?>
                </li>
                <li class="site-footer-list-item site-footer-list-item-right">
                    <h4 class="title-fourth">
                        <?php echo  $menuContact->name ?>
                    </h4>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-contact', 'menu_class' => 'footer-nav-menu' ) ); ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="grid">
        <div class="col-3-12">
            &nbsp;
        </div>
        <div class="col-9-12 site-footer-cta-wrapper">
            <div class="site-footer-cta">
                <ul class="site-footer-cta-list">
                    <?php query_posts(array('post_type' => 'cta','orderby' => 'menu')); if(have_posts()) : while(have_posts()) : the_post();
                        get_template_part( 'content/content', 'cta' );
                    endwhile; endif; wp_reset_query(); ?>
                </ul>
            </div>
            <div class="site-footer-copyright">
                <?php
                if (get_option("copyright") !== false) {
                    echo get_option("copyright");
                }
                ?>
            </div>
        </div>
    </div>
</footer>

</div>
    <?php wp_footer(); ?>
</body>
</html>