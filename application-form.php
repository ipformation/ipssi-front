<?php
require_once('wp-load.php');
require_once(ABSPATH . "wp-admin" . '/includes/image.php');
require_once(ABSPATH . "wp-admin" . '/includes/file.php');
require_once(ABSPATH . "wp-admin" . '/includes/media.php');

$siteUrl =  get_site_url();
$data = $_POST;

// The ID of the post this attachment is for.
$post_id = $data['offreId'];

$erp_offer_id = $data['erp_offer_id'];
$nom_candidate = $data['nom'];
$prenom_candidate = $data['prenom'];
$email_candidate = $data['email'];
$metier_candidate = $data['metier'];

// Get the path to the upload directory.
$wp_upload_dir = wp_upload_dir();

//Catch Temp Folder
$image_url = $_FILES['cv']['tmp_name'];
$image_data = file_get_contents($image_url);

$filename = $_FILES['cv']['name'];
//Delete extension
$noExtensionFilename = preg_replace( '/\.[^.]+$/', '', $filename );
//Unique filename + force extension
$cryptFileName = 'ipssi-cv-offer-' . $erp_offer_id . '-' . substr(md5(uniqid(rand(), true)), 0, 8) . '.pdf';
//Manage upload dir
$file = $wp_upload_dir['basedir'] . '/application/' . $cryptFileName;

file_put_contents($file, $image_data);

// The ID of the post this attachment is for.
$parent_post_id = $data['offreId'];

// Check the type of file. We'll use this as the 'post_mime_type'.
$filetype = wp_check_filetype( basename( $file ), null );

// Prepare an array of post data for the attachment.
$attachment = array(
	'guid'           => $wp_upload_dir['baseurl'] . '/application/' . basename( $file ),
	'post_mime_type' => $filetype['type'],
	'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file ) ),
	'post_content'   => $erp_offer_id . ',' . $nom_candidate . ',' . $prenom_candidate . ',' . $email_candidate . ',' . $metier_candidate,
	'post_status'    => 'inherit'
);

// Insert the attachment.
$attach_id = wp_insert_attachment( $attachment, $file );

// Generate the metadata for the attachment, and update the database record.
$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
wp_update_attachment_metadata( $attach_id, $attach_data );


$postUrl  = $siteUrl . '?p=' . $post_id;
header("Location: $postUrl");



