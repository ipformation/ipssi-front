<?php
require_once('wp-load.php');
$pagesPublished = get_posts(
    array (
        'post_status' => 'publish',
        'post_type' => 'application'
    )
);

$pagesPending = get_posts(
    array (
        'post_status' => 'pending',
        'post_type' => 'application'
    )
);

$id = $_GET['id'];
$url = 'http://projet-ipssi.com/app_dev.php/offer/rest/offer/'.$id;

/* gets the CVs from a URL */
$ch = curl_init();
$timeout = 5;
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$data = curl_exec($ch);
curl_close($ch);

$datadecode = json_decode($data);

if ($datadecode->available){
    if (!empty($pagesPublished)){
        foreach($pagesPublished as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['offer_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $offer_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_content' => $datadecode->description,
                    'post_type' => 'application',
                    'post_name' => $datadecode->title,
                    'post_status' => 'publish'
                );
                wp_insert_post($offer_post);
                update_post_meta( $page->ID, 'content', $datadecode->description );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    } elseif (!empty($pagesPending)) {
        foreach($pagesPending as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['offer_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $offer_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_content' => $datadecode->description,
                    'post_type' => 'application',
                    'post_name' => $datadecode->title,
                    'post_status' => 'publish'
                );
                wp_insert_post($offer_post);
                update_post_meta( $page->ID, 'content', $datadecode->description );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    }
} else {
    if (!empty($pagesPending)){
        foreach($pagesPending as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['offer_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $offer_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_content' => $datadecode->description,
                    'post_type' => 'application',
                    'post_name' => $datadecode->title,
                    'post_status' => 'pending'
                );
                wp_insert_post($offer_post);
                update_post_meta( $page->ID, 'content', $datadecode->description );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    } elseif (!empty($pagesPublished)) {
        foreach($pagesPublished as $page){
            $fields = get_post_custom($page->ID);
            $postID = $fields['offer_id'][0];
            //Compare ID from GET
            if (isset($postID) && ($postID == $_GET['id'])){
                $offer_post = array(
                    'ID' => $page->ID,
                    'post_author' => 1,
                    'post_title' => $datadecode->title,
                    'post_content' => $datadecode->description,
                    'post_type' => 'application',
                    'post_name' => $datadecode->title,
                    'post_status' => 'pending'
                );
                wp_insert_post($offer_post);
                update_post_meta( $page->ID, 'content', $datadecode->description );
                update_post_meta( $page->ID, 'title', $datadecode->title );
            }
        }
    }
}







